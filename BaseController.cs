﻿using Microsoft.AspNetCore.Mvc;

namespace ejdahaorder_backend
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
            
    }
}
