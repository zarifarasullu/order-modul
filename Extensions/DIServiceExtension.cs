﻿using AutoMapper;
using ejdahaorder_backend.FoodCategories;
using ejdahaorder_backend.FoodCategories.Dto;
using ejdahaorder_backend.FoodCategories.Validators;
using ejdahaorder_backend.Foods;
using ejdahaorder_backend.Foods.Dto;
using ejdahaorder_backend.Foods.Validators;
using ejdahaorder_backend.Helpers;
using ejdahaorder_backend.Orders;
using ejdahaorder_backend.Orders.Dto;
using ejdahaorder_backend.Orders.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace ejdahaorder_backend.Extensions
{
    public static class DIServiceExtension
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddTransient<IFoodCategoryService, FoodCategoryService>();
            services.AddTransient<IFoodService, FoodService>();
            services.AddTransient<IOrderService, OrderService>();
        }
        public static void RegisterCustomValidators(this IServiceCollection services)
        {
            #region FoodCategories
            services.AddTransient<IValidator<FoodCategoryCreateDto>, FoodCategoryCreateValidator>();
            services.AddTransient<IValidator<FoodCategoryUpdateDto>, FoodCategoryUpdateValidator>();
            
            #endregion

            #region Foods
            services.AddTransient<IValidator<FoodCreateDto>, FoodCreateValidator>();
            services.AddTransient<IValidator<FoodUpdateDto>, FoodUpdateValidator>();
            #endregion

            #region Orders
            services.AddTransient<IValidator<OrderCreateDto>, OrderCreateValidator>();
            services.AddTransient<IValidator<OrderUpdateDto>, OrderUpdateValidator>();
            #endregion
                        

        }
        public static void RegisterCustomMappers(this IServiceCollection services)
        {
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new OrderMappingProfile());
                mc.AddProfile(new FoodCategoryMappingProfile());
                mc.AddProfile(new FoodMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

        }

        public static void RegisterCustomHelpers(this IServiceCollection services)
        {
            services.AddTransient<IEmailHelper, EmailHelper>();
            services.AddTransient<SmsHelper, SmsHelper>();
        }


    }

}
