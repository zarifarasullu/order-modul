﻿using ejdahaorder_backend.Foods.Domain;
using ejdahaorder_backend.Orders.Domain;
using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.OrderItems.Domain
{
    public class OrderItem: BaseEntity
    {
        public int FoodId { get; set; }
        public  Food Food { get; set; }

        public int OrderId { get; set; }
        public  Order Order { get; set; }

        public int Quantity { get; set; }

    }
}
