﻿using System;
using System.Collections.Generic;

namespace ejdahaorder_backend.Orders.Dto
{
    public class OrderCreateDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public DateTime OrderDate { get; set; }
        public float TotalPrice { get; set; }

        public Dictionary<int, int> Foods { get; set; }


    }
}
