﻿using ejdahaorder_backend.Shared;
using System;

namespace ejdahaorder_backend.Orders.Dto
{
    public class OrderDto: BaseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public DateTime OrderDate { get; set; }
        public float TotalPrice { get; set; }


    }
}
