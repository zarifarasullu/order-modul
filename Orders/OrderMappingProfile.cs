﻿using AutoMapper;
using ejdahaorder_backend.Orders.Domain;
using ejdahaorder_backend.Orders.Dto;

namespace ejdahaorder_backend.Orders
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<Order, OrderDto>();
            CreateMap<OrderCreateDto, Order>();
            CreateMap<OrderUpdateDto, Order>().ReverseMap();
        }
    }
}
