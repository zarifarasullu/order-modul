﻿using ejdahaorder_backend.Orders.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ejdahaorder_backend.Orders
{

    public class OrdersController : BaseController
    {
        private readonly IOrderService orderService;

        public OrdersController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpGet]
        public async Task<IActionResult> FindListAsync()
        {
            return Ok(await orderService.FindListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> FindByIdAsync(int id)
        {
            return Ok(await orderService.FindByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] OrderCreateDto orderCreateDto)
        {
            await orderService.CreateAsync(orderCreateDto);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] OrderUpdateDto orderUpdateDto)
        {
            await orderService.UpdateAsync(orderUpdateDto);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await orderService.DeleteAsync(id);
            return Ok();
        }
    }
}
