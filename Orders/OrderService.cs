﻿using AutoMapper;
using ejdahaorder_backend.Data;
using ejdahaorder_backend.Helpers;
using ejdahaorder_backend.OrderItems.Domain;
using ejdahaorder_backend.Orders.Domain;
using ejdahaorder_backend.Orders.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
namespace ejdahaorder_backend.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IMapper mapper;
        private readonly DataContext dataContext;
        private readonly IEmailHelper emailHelper;
        private readonly SmsHelper smsHelper;
        public OrderService(DataContext dataContext, IMapper mapper, IEmailHelper emailHelper, SmsHelper smsHelper)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;
            this.emailHelper = emailHelper;
            this.smsHelper = smsHelper;

        }

        public async Task<IEnumerable<OrderDto>> FindListAsync()
        {
            return mapper.Map<IList<OrderDto>>(await dataContext.Orders.ToListAsync());
        }

        public async Task<OrderUpdateDto> FindByIdAsync(int id)
        {
            return mapper.Map<OrderUpdateDto>(await dataContext.Orders.FirstOrDefaultAsync(e => e.Id == id));
        }

        public async Task CreateAsync(OrderCreateDto orderCreateDto)
        {
            using (TransactionScope transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                EntityEntry<Order> order = await dataContext.Orders.AddAsync(mapper.Map<Order>(orderCreateDto));

                await dataContext.SaveChangesAsync();

                int orderId = order.Entity.Id;

                if (orderCreateDto.Foods != null)
                {
                    IList<OrderItem> orderItems = new List<OrderItem>();
                    Dictionary<int, int> OrderedFoods = orderCreateDto.Foods;

                    for (int i = 0; i < OrderedFoods.Count; i++)
                    {
                        orderItems.Add(new OrderItem()
                        {
                            FoodId = OrderedFoods.ElementAt(i).Key,
                            OrderId = orderId,
                            Quantity = OrderedFoods.ElementAt(i).Value,
                        });
                    }

                    List<string> foods = new List<string>();
                    List<int> foodCounts = new List<int>();
                    List<double> foodPrice = new List<double>();
                    string orderAndCount = "";
                    string orderAndCountSms = "";
                    if (orderItems.Count > 0)
                    {

                        for (int i = 0; i < orderItems.Count; i++)
                        {
                            foods.AddRange(dataContext.Foods.Where(x => x.Id == orderItems[i].FoodId).Select(el => el.Name));
                            foodPrice.AddRange(dataContext.Foods.Where(x => x.Id == orderItems[i].FoodId).Select(el => el.UnitPrice));
                            foodCounts.AddRange(orderCreateDto.Foods.Values);
                            orderAndCount += foodCounts[i] + " ədəd " + foods[i] + "<br>";
                            orderAndCountSms += foodCounts[i] + " ədəd-" + foods[i] + "; ";
                        }

                        string body = "<html style='background-color:blue'>Sifariş <b>" + orderCreateDto.FirstName
                            + " " + orderCreateDto.LastName
                            + "</b> tərəfindən <b>" + orderCreateDto.OrderDate
                            + "</b> tarixində həyata keçirilib. <br> <hr> Sifarişə daxildir: <br>"
                            + orderAndCount + "<br>Ümumi borc: " + orderCreateDto.TotalPrice + " AZN</html>";


                        await dataContext.OrderItems.AddRangeAsync(orderItems);
                        await dataContext.SaveChangesAsync();
                        emailHelper.Send(orderCreateDto.Email, "Sifariş Haqqında Məlumat", body);
                        string body2 = "Sifariş edən: "+ orderCreateDto.FirstName + " " + orderCreateDto.LastName+ " Tarix: "+ orderCreateDto.OrderDate + " Daxildir:" + orderAndCountSms + " Borc: " + orderCreateDto.TotalPrice + " AZN";
                        smsHelper.Send(orderCreateDto.PhoneNumber,body2);
                        
                    }
                }

                transaction.Complete();
            }
        }

        public async Task UpdateAsync(OrderUpdateDto orderUpdateDto)
        {
            using (TransactionScope transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                Order order = await dataContext.Orders.FindAsync(orderUpdateDto.Id);
                dataContext.Entry(order).CurrentValues.SetValues(mapper.Map<Order>(orderUpdateDto));



                IList<OrderItem> orderItems = await dataContext.OrderItems.Where(e => e.OrderId == orderUpdateDto.Id).ToListAsync();
                dataContext.OrderItems.RemoveRange(orderItems);
                IList<OrderItem> newOrderItems = new List<OrderItem>();

                orderUpdateDto.Foods.ToList().ForEach(food =>
                {
                    newOrderItems.Add(new OrderItem()
                    {
                        FoodId = food.Key,
                        OrderId = orderUpdateDto.Id,
                        Quantity = food.Value
                    });
                });


                await dataContext.OrderItems.AddRangeAsync(newOrderItems);
                await dataContext.SaveChangesAsync();

                transaction.Complete();
            }
        }

        public async Task DeleteAsync(int id)
        {
            Order order = await dataContext.Orders.FindAsync(id);
            dataContext.Orders.Remove(order);
            await dataContext.SaveChangesAsync();
        }
    }
}
