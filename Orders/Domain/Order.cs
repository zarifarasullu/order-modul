﻿using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.Orders.Domain
{
    public class Order: BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string OrderDate { get; set; }
        public float TotalPrice { get; set; }
    }
}
