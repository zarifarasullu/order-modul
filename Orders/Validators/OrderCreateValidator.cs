﻿using ejdahaorder_backend.Orders.Dto;
using FluentValidation;
using System.Text.RegularExpressions;

namespace ejdahaorder_backend.Orders.Validators
{
    public class OrderCreateValidator : AbstractValidator<OrderCreateDto>
    {
        public OrderCreateValidator()
        {
            RuleFor(e => e.FirstName).NotNull().NotEmpty().WithMessage("Firstname is required!");
            RuleFor(e => e.LastName).NotNull().NotEmpty().WithMessage("Lastname is required!");
            RuleFor(e => e.Email).NotNull().NotEmpty().WithMessage("Email is required!");
            RuleFor(e => e.PhoneNumber).NotNull().NotEmpty().WithMessage("Phone number is required!");
            RuleFor(e => e.Address).NotNull().NotEmpty().WithMessage("Address is required!");
            RuleFor(e => e.OrderDate).NotNull().NotEmpty().WithMessage("Order Date is required!");
            RuleFor(e => e.Foods).NotNull().NotEmpty().WithMessage("Foods List is required!");
            RuleFor(e => e.Foods.Keys).NotNull().NotEmpty().WithMessage("Food is required!");
            RuleFor(e => e.Foods.Values).NotNull().NotEmpty().WithMessage("Quantity is required!");

            RuleFor(e => e.Email).EmailAddress().Must(EmailType).WithMessage("Email is incorrect!");
            RuleFor(e => e.Address).Length(10, 1000).WithMessage("Address length must be greater than 10 symbols");
            RuleFor(e => e.PhoneNumber).Length(9).WithMessage("Phone number must be 9 numeric symbols as 55 123 45 67");

        }
        private bool EmailType(string email)
        {
            //return Regex.Matches(email, @"^\w+(\.\w+)*(\-\w+)*@[a-zA-Z]+\.[a-zA-Z]+$").Count>0;
            Regex regex = new Regex(@"^\w+([.-]\w+)*@[a-zA-Z]+\.[a-zA-Z]{2,3}$");
            return regex.Matches(email).Count > 0;

        }

    }
}
