﻿namespace ejdahaorder_backend.Helpers
{
    public interface IEmailHelper
    {
        void Send(string to, string subject, string body);
    }

}
