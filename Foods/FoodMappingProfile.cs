﻿using AutoMapper;
using ejdahaorder_backend.FoodCategories.Domain;
using ejdahaorder_backend.Foods.Domain;
using ejdahaorder_backend.Foods.Dto;

namespace ejdahaorder_backend.Foods
{
    public class FoodMappingProfile:Profile
    {
        public FoodMappingProfile()
        {
            CreateMap<Food, FoodDto>();
            CreateMap<FoodCreateDto, Food>().ForMember(s => s.FoodCategory, d => d.MapFrom(e => new FoodCategory() { Id = e.FoodCategoryId })); ;
            CreateMap<FoodUpdateDto, Food>().ReverseMap().ForMember(s => s.FoodCategoryId, d => d.MapFrom(e => e.FoodCategory != null ? e.FoodCategory.Id : default(int)));
        }
    }
}
