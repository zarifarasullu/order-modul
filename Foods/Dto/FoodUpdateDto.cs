﻿using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.Foods.Dto
{
    public class FoodUpdateDto:BaseDto
    {
        public string Name { get; set; }
        public double UnitPrice { get; set; }

        public int FoodCategoryId { get; set; }
        
        public string Image { get; set; }
        public string Description { get; set; }
        public object FoodCategory { get; internal set; }
    }
}
