﻿using ejdahaorder_backend.FoodCategories.Dto;
using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.Foods.Dto
{
    public class FoodDto: BaseDto
    {
        public string Name { get; set; }
        public double UnitPrice { get; set; }

        public FoodCategoryDto FoodCategory { get; set; }
        

        public string Image { get; set; }
        public string Description { get; set; }
    }
}
