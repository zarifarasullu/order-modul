﻿using ejdahaorder_backend.FoodCategories.Domain;
using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.Foods.Domain
{
    public class Food:BaseEntity
    {        
        public string Name { get; set; }
        public double UnitPrice { get; set; }

        public int FoodCategoryId { get; set; }
        public FoodCategory FoodCategory { get; set; }

        public string Image { get; set; }
        public string Description { get; set; }
    }
}
