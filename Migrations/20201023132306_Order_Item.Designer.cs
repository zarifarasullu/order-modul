﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ejdahaorder_backend.Data;

namespace ejdahaorder_backend.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20201023132306_Order_Item")]
    partial class Order_Item
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ejdahaorder_backend.FoodCategories.Domain.FoodCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id")
                        .HasName("pk_food_categories");

                    b.HasIndex("Name")
                        .IsUnique()
                        .HasName("uk_food_categories_name");

                    b.ToTable("food_categories");
                });

            modelBuilder.Entity("ejdahaorder_backend.Foods.Domain.Food", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("FoodCategoryId")
                        .HasColumnName("food_category_id")
                        .HasColumnType("int");

                    b.Property<string>("Image")
                        .HasColumnName("image")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("UnitPrice")
                        .HasColumnName("unit_price")
                        .HasColumnType("float");

                    b.HasKey("Id")
                        .HasName("pk_foods");

                    b.HasIndex("FoodCategoryId");

                    b.ToTable("foods");
                });

            modelBuilder.Entity("ejdahaorder_backend.Items.Domain.Item", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("FoodId")
                        .HasColumnName("food_id")
                        .HasColumnType("int");

                    b.Property<int>("Quantity")
                        .HasColumnName("quantity")
                        .HasColumnType("int");

                    b.HasKey("Id")
                        .HasName("pk_items");

                    b.HasIndex("FoodId");

                    b.ToTable("items");
                });

            modelBuilder.Entity("ejdahaorder_backend.OrderItems.Domain.OrderItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ItemId")
                        .HasColumnName("item_id")
                        .HasColumnType("int");

                    b.Property<int>("OrderId")
                        .HasColumnName("order_id")
                        .HasColumnType("int");

                    b.HasKey("Id")
                        .HasName("pk_order_items");

                    b.HasIndex("ItemId");

                    b.HasIndex("OrderId", "ItemId")
                        .IsUnique()
                        .HasName("uk_order_items_order_id_item_id");

                    b.ToTable("order_items");
                });

            modelBuilder.Entity("ejdahaorder_backend.Orders.Domain.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnName("address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnName("email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnName("first_name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnName("last_name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasColumnName("phone_number")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id")
                        .HasName("pk_orders");

                    b.ToTable("orders");
                });

            modelBuilder.Entity("ejdahaorder_backend.Foods.Domain.Food", b =>
                {
                    b.HasOne("ejdahaorder_backend.FoodCategories.Domain.FoodCategory", "FoodCategory")
                        .WithMany()
                        .HasForeignKey("FoodCategoryId")
                        .HasConstraintName("fk_foods_food_categories_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ejdahaorder_backend.Items.Domain.Item", b =>
                {
                    b.HasOne("ejdahaorder_backend.Foods.Domain.Food", "Food")
                        .WithMany()
                        .HasForeignKey("FoodId")
                        .HasConstraintName("fk_items_foods_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ejdahaorder_backend.OrderItems.Domain.OrderItem", b =>
                {
                    b.HasOne("ejdahaorder_backend.Items.Domain.Item", "Item")
                        .WithMany()
                        .HasForeignKey("ItemId")
                        .HasConstraintName("fk_order_items_item_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ejdahaorder_backend.Orders.Domain.Order", "Order")
                        .WithMany()
                        .HasForeignKey("OrderId")
                        .HasConstraintName("fk_order_items_order_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
