﻿using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.FoodCategories.Dto
{
    public class FoodCategoryUpdateDto:BaseDto
    {
        public string Name { get; set; }
    }
}
