﻿using AutoMapper;
using ejdahaorder_backend.FoodCategories.Domain;
using ejdahaorder_backend.FoodCategories.Dto;

namespace ejdahaorder_backend.FoodCategories
{
    public class FoodCategoryMappingProfile:Profile
    {
        public FoodCategoryMappingProfile()
        {
            CreateMap<FoodCategory, FoodCategoryDto>();
            CreateMap<FoodCategoryCreateDto, FoodCategory>();
            CreateMap<FoodCategoryUpdateDto, FoodCategory>().ReverseMap();
        }
    }
}
