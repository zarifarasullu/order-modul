﻿using AutoMapper;
using ejdahaorder_backend.Data;
using ejdahaorder_backend.FoodCategories.Domain;
using ejdahaorder_backend.FoodCategories.Dto;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ejdahaorder_backend.FoodCategories
{
    public class FoodCategoryService : IFoodCategoryService
    {
        private readonly IMapper mapper;
        private readonly DataContext dataContext;
        public FoodCategoryService(DataContext dataContext, IMapper mapper)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<FoodCategoryDto>> FindListAsync()
        {
            return mapper.Map<IList<FoodCategoryDto>>(await dataContext.FoodCategories.ToListAsync());
        }

        public async Task CreateAsync(FoodCategoryCreateDto foodCategoryCreateDto)
        {
            dataContext.FoodCategories.Add(mapper.Map<FoodCategory>(foodCategoryCreateDto));

            await dataContext.SaveChangesAsync();
        }
               
        public async Task UpdateAsync(FoodCategoryUpdateDto foodCategoryUpdateDto)
        {
            FoodCategory foodCategory = await dataContext.FoodCategories.FindAsync(foodCategoryUpdateDto.Id);
            dataContext.Entry(foodCategory).CurrentValues.SetValues(mapper.Map<FoodCategory>(foodCategoryUpdateDto));

            await dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            FoodCategory foodCategory = await dataContext.FoodCategories.FindAsync(id);
            dataContext.FoodCategories.Remove(foodCategory);

            await dataContext.SaveChangesAsync();
        }

    }
}
